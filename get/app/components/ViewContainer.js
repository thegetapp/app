'use strict'

import {View} from 'react-native'
import React, {Component} from 'react'

class ViewContainer extends Component {
  render(){
    return (
      <View>
        {this.props.children}
      </View>
    )
  }
}

module.exports = ViewContainer
