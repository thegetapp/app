'use strict'

import {Navigator, Text} from 'react-native'
import React, {Component} from 'react'
import FirstRecommendationsScreen from '../screens/FirstRecommendationsScreen'

class AppNavigator extends Component {

  _renderScene(route, navigator) {
    var globalNavigatorProps = {navigator}

    switch(route.ident){
      case "FirstRecommendations":
        return (
          <FirstRecommendationsScreen
            { ...globalNavigatorProps}
          />
        )
      default:
        return (
          <Text>{'You did something wrong! ' + JSON.stringify(route) }</Text>
        )
    }
  }

  render(){
    return (
      <Navigator
        initialRoute = {this.props.initialRoute}
        ref = "appNavigator"
        renderScene = {this._renderScene}
        configureScene = {(route) => ({
          ...route.sceneConfig || Navigator.SceneConfigs.FloatFromRight})} />
    )
  }
}


module.exports = AppNavigator
