'use strict'

import {Text, View, ListView, TouchableOpacity, Navigator} from 'react-native'
import ViewContainer from '../components/ViewContainer'
import React, {Component} from 'react'
import _ from 'lodash'

const recommendations = [
  {title: "Cafe", location: "Copenhagen"},
  {title: "Restaurant", location: "Copenhagen"},
  {title: "Simple Bar", location: "Copenhagen"}
]

class FirstRecommendationsScreen extends Component {
  constructor(props) {
    super(props)
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2})
    this.state = {
      recommendationsDataSource: ds.cloneWithRows(recommendations)
    }
  }

  render(){
    return (
      <ViewContainer>
        <ListView
          style={{marginTop: 100}}
          initialListSize={10}
          dataSource={this.state.recommendationsDataSource}
          renderRow={(recommendation) => {return this._renderRecommendationRow(recommendation) }}
        />
      </ViewContainer>
    )
  }

  _renderRecommendationRow(recommendation) {
    return (
      <TouchableOpacity onPress={(event) => this._navigateToRecommendationShow(recommendation)}>
        <Text style={{flex: 1}}>{recommendation.title}</Text>
      </TouchableOpacity>
    )
  }

  _navigateToRecommendationShow(recommendation) {
    this.props.navigator.push({
      ident: "FirstRecommendations",
      recommendation
    })
  }

}

module.exports = FirstRecommendationsScreen
