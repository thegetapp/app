'use strict'

import React, {Component} from 'react'
import {ListView} from 'react-native'
import _ from 'lodash'

const items = [
  {title: "Home", route: "FirstRecommendations"},
  {title: "Settings", route: "Settings"}
]
class DrawerMenu extends Component {

  constructor(props) {
    super(props)
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2})
    this.state = {
      recommendationsDataSource: ds.cloneWithRows(recommendations)
    }
  }

  render(){
    return (
      <ViewContainer>
        <ListView
          style={{marginTop: 100}}
          initialListSize={10}
          dataSource={this.state.recommendationsDataSource}
          renderRow={(recommendation) => {return this._renderRecommendationRow(recommendation) }}
        />
      </ViewContainer>
    )
  }

  _renderDrawerRow(item) {
    return (
      <TouchableOpacity onPress={(event) => this._navigateToDrawerItem(item)}>
        <Text style={{flex: 1}}>{item.title}</Text>
      </TouchableOpacity>
    )
  }

  _navigateToDrawerItem(item) {
    this.props.navigator.push({
      ident: item.path,
      item
    })
  }
}
