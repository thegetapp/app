/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import AppNavigator from './app/navigation/AppNavigator'
import Drawer from './app/screens/Drawer'
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  DrawerLayoutAndroid,
} from 'react-native';

class get extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedTab: "tab1"
    }
  }

  render() {
    var globalNavigatorProps = {navigator}
    
    var navigationView = (
      <Drawer
        { ...globalNavigatorProps}
      />
    )

    return (
      <DrawerLayoutAndroid
        drawerWidth={300}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={() => navigationView}>
      </DrawerLayoutAndroid>
    )
  }
}



AppRegistry.registerComponent('get', () => get);
